<html>

<head>
    <title>Contact Us</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/library/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>

<body>

<div class="container">

    <div class="row">

        <div class="col-xl-8 offset-xl-2 py-5" id="form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <p class="text-muted">
                        <strong>*</strong> These fields are required.</p>
                </div>
            </div>
            <div id="messages"></div>
            <form id="contact-form" method="post" action="/api/contact-form.php" role="form">
                <div class="controls">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_name">Firstname *</label>
                                <input id="form_name" type="text" name="firstname" class="form-control"
                                       placeholder="Please enter your firstname *"
                                       data-error="Firstname is required.">
                                <div class="help-block "></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_email">Email *</label>
                                <input id="form_email" type="email" name="email" class="form-control"
                                       placeholder="Please enter your email *"
                                       data-error="Valid email is required.">
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="form_message">Message *</label>
                                <textarea id="form_message" name="message" class="form-control"
                                          placeholder="Message for me *" rows="4"
                                          data-error="Please, leave us a message."></textarea>
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-success btn-send" value="Send message">
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<script src="/js/library/jquery.min.js"></script>
<script src="/js/library/popper.min.js"></script>
<script src="/js/library/bootstrap.min.js"></script>
<script src="/js/contact-form.js"></script>
</body>

</html>