<?php


class ContactUs
{
    function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    function create($data)
    {
        $sql = "INSERT INTO contact_us (firstname, email, message) VALUES (:firstname, :email, :message)";
        $stmt= $this->pdo->prepare($sql);
        $stmt->execute($data);
    }
}