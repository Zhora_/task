<?php
require_once "db/ConnectDb.php";
require_once "models/ContactUs.php";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    header('HTTP/1.1 200 OK');

    if (!isset($_POST['firstname'], $_POST['email'], $_POST['message'])) {
        header('HTTP/1.1 400 Bad Request');
        sendResponse(400, "firstname, email, message are required.");
    }
    $errors = [];

    $firstname = htmlentities(filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING));
    $email = htmlentities(filter_var(filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL));
    $message = htmlentities(filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING));

    if (strlen($firstname) === 0) {
        $errors[] = "Firstname is required.";
    }

    if (!$email) {
        $errors[] = "Email is invalid.";
    }

    if (strlen($message) === 0) {
        $errors[] = "Message is required.";
    }

    if (count($errors) > 0) {
        sendResponse(400, $errors);
    }


    $model = new ContactUs($pdo);
    //Insert row
    $model->create([
        "firstname" => $firstname,
        "email" => $email,
        "message" => $message
    ]);

    sendResponse(200, "Thank you " . $firstname);
} else {
    sendResponse(400, "Only Post method allowed");
}


function sendResponse($status, $message)
{
    $headers = [
        200 => 'HTTP/1.1 200 OK',
        400 => 'HTTP/1.1 400 Bad Request',
        404 => 'HTTP/1.1 404 Not Found'
    ];

    header($headers[$status]);

    die(json_encode(["message" => $message]));
}