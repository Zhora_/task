let Validator = {
    errors: [],
    firstname: function (input) {
        if (!input.value.length) {
            this.errors.push({
                name: "firstname",
                message: input.getAttribute("data-error")
            })
        }
    },
    email: function (input) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test((input.value).toLowerCase())) {
            this.errors.push({
                name: "email",
                message: input.getAttribute("data-error")
            })
        }
    },
    message: function (input) {
        if (!input.value.length) {
            this.errors.push({
                name: "message",
                message: input.getAttribute("data-error")
            })
        }
    },
    clearErrors: function () {
        this.errors = [];
        $(".help-block").html("");
    }
};

$(document).ready(function () {
    $("#contact-form").on("submit", function (e) {
        let _self = $(this);
        e.preventDefault();
        //reset errors array
        Validator.clearErrors();


        let inputs = $(this).find('input,textarea');

        inputs.each(function (i, input) {
            let name = input.getAttribute('name');
            if (!name) return;

            Validator[name](input);
        });

        if (Validator.errors.length) {
            Validator.errors.forEach((field) => {
                console.log(field)
                $(`input[name=${field.name}],textarea[name=${field.name}]`)
                    .siblings(".help-block")
                    .text(field.message)
            });

            return;
        }

        $.ajax({
            url: _self.attr("action"),
            type: _self.attr("method"),
            data: _self.serialize(),
            dataType: "json",
            success: function (resp) {
                $("#form-wrapper").remove();

                $("<div>", {"class": "alert alert-success m-auto my-4"})
                    .html(resp.message)
                    .appendTo(".row");
            },
            error: function (err) {
                let errors = JSON.parse(err.responseText);
                $("#messages").html(errors.message.map(error => `<p class="text-danger">${error}</p>`))
            }
        })
    })
});